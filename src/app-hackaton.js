import { LitElement, html } from 'lit-element';
import './login-hackaton/login-hackaton.js';
import './main-hackaton/main-hackaton.js';

export class AppHackaton extends LitElement {
    
    static get properties() {
        return {
            booMainForm: {type: Boolean}
        };
    }
    constructor() {
        super();
        this.booMainForm = false;
    }
    render() {
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <login-hackaton id="loginForm" @user-login="${this.userLogin}"></login-hackaton>
        <main-hackaton id="mainForm"></main-hackaton>
        `;
    }
    updated(changedProperties) {
        console.log("updated");
        if (changedProperties.has("booMainForm")) {
            this.shadowRoot.getElementById("loginForm").classList.remove("d-none");
            this.shadowRoot.getElementById("mainForm").classList.add("d-none");
        }
    }
    userLogin(e) {
        console.log("userLogin en app-hackaton");
        this.shadowRoot.getElementById("loginForm").classList.add("d-none");
        this.shadowRoot.getElementById("mainForm").classList.remove("d-none");
        this.shadowRoot.querySelector("main-hackaton").showForm = true;
    }
}
customElements.define('app-hackaton', AppHackaton);

