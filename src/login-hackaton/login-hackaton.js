import { LitElement, html, css } from 'lit-element';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text.js';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js';
import '@bbva-web-components/bbva-web-form-password/bbva-web-form-password.js';

export class LoginHackaton extends LitElement {
    
    static getStyles(){
      return css`
        div.entrada {
          width: 80%;
          max-width: 400px;
          margin: 20px auto 0px auto;
        }
      `;
    }
    static get properties() {
      return {
          user: {type: Object}
      };
    }
    constructor() {
      super();
      this.user = {
        name: "",
        code: "",
        pass: ""
      };
    }

    render() {
      return html`
      <div>
        <div style="text-align: center;">
	        <img src="./img/BBVA.jpg" alt="BBVA" style="width: 50%; max-width: 250px;">
        </div>
        <div class="entrada">
          <bbva-web-form-text label="Ingresar Usuario" @change=${this.setUser}></bbva-web-form-text>
        </div>
        <div class="entrada" class="case">
          <bbva-web-form-password label="Ingresar Contraseña" @change=${this.setPassword}></bbva-web-form-password>
        </div>
        <div class="entrada" style="text-align: center;">
          <bbva-web-button-default @click="${this.login}">Entrar</bbva-web-button-default>
        </div>
      </div>
      `;
    }

    login(){
      console.log("login");
      this.dispatchEvent(
        new CustomEvent ("user-login", {
                detail: {
                    name: "javier"
                }
            }
        )
    );
      console.log("final");
      /*let xhr = new XMLHttpRequest();
      xhr.onload = () => {
          if (xhr.status === 200){
              console.log("Petición completada correctamente");
              let apiResponse = JSON.parse(xhr.responseText);
              this.user.name = apiResponse.name;
          }
      }

      xhr.open("GET","https://run.mocky.io/v3/cdedf0ed-2a80-4ded-bdb7-f3437ff59ebb");
      xhr.send();*/
  }
}
customElements.define('login-hackaton', LoginHackaton);