import { LitElement, html } from 'lit-element';
import '../sm-hackaton/sm-hackaton.js';

export class MainHackaton extends LitElement {

    static get properties() {
        return {
            showForm: {type: Boolean}
        };
    }
    constructor() {
        super();
        this.showForm = false;
    }

    render() {
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <sm-hackaton id="firstForm"></sm-hackaton>
        `;
    }

    showLogin() {
        console.log("showLogin");
        this.shadowRoot.getElementById("firstForm").classList.remove("d-none");
    }
}
customElements.define('main-hackaton', MainHackaton);