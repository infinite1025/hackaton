import { LitElement, html } from 'lit-element';

export class SMHackaton extends LitElement {

    static getStyles(){
        return css`
          div.entrada {
            width: 80%;
            max-width: 400px;
            margin: 20px auto 0px auto;
          }
        `;
      }
      static get properties() {
        return {
            sm: {type: Object}
        };
      }
      constructor() {
        super();
        this.sm = {
          name: "",
          code: "",
          pass: ""
        };
      }
  
      render() {
        return html`
        <div>
          <div class="entrada">
            <bbva-web-form-text @change=${this.setDNI}  label="Ingresar SDA"></bbva-web-form-text>
          </div>
          <div class="entrada">
            <bbva-web-form-text label="Ingresar Usuario" @change=${this.setUser}></bbva-web-form-text>
          </div>
          <div class="entrada" class="case">
            <bbva-web-form-password label="Ingresar Contraseña" @change=${this.setPassword}></bbva-web-form-password>
          </div>
          <div class="entrada" style="text-align: center;">
            <bbva-web-button-default @click="${this.login}">Entrar</bbva-web-button-default>
          </div>
        </div>
        `;
      }
  
}
customElements.define('sm-hackaton', SMHackaton);